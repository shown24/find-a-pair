import {connect} from 'react-redux';
import {Dispatch} from 'redux';

import {StoreState} from '../types';
import * as actions from '../actions';

import {Cards} from "../components/Cards";

export function mapStateToProps({difficultLevel, gameIsRunning}: StoreState) {
  return {difficultLevel, gameIsRunning};
}

export function mapDispatchToProps(dispatch: Dispatch<actions.GameAction>) {
  return {
    onFindAllPairs: () => dispatch(actions.findAllPairs())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
