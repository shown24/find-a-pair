import {connect} from 'react-redux';
import {Dispatch} from 'redux';

import {StoreState} from '../types';
import * as actions from '../actions';

import {Menu} from "../components/Menu";

export function mapStateToProps({difficultLevel, gameIsRunning}: StoreState) {
  return {difficultLevel, gameIsRunning};
}

export function mapDispatchToProps(dispatch: Dispatch<actions.GameAction | actions.ChangeLevel>) {
  return {
    onStartGame: () => dispatch(actions.startGame()),
    onChangeLevel: (payload: number) => dispatch(actions.changeLevel(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
