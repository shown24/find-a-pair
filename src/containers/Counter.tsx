import {connect} from 'react-redux';
import {Dispatch} from 'redux';

import {StoreState} from '../types';
import * as actions from '../actions';

import {Counter} from "../components/Counter";

export function mapStateToProps({gameIsRunning, findAllPairs}: StoreState) {
  return {gameIsRunning, findAllPairs};
}

export function mapDispatchToProps(dispatch: Dispatch<actions.GameAction>) {
  return {
    onStopGame: () => dispatch(actions.stopGame())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
