import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';

import {createStore} from 'redux';
import {game} from './reducers';
import {StoreState} from './types';
import {GameAction} from "./actions";
import GameArea from "./App";

const store = createStore<StoreState, GameAction, null, null>(game, {
  gameIsRunning: false,
  difficultLevel: 6,
});

ReactDOM.render(
  <Provider store={store}>
    <GameArea/>
  </Provider>,
  document.getElementById('findThePair') as HTMLElement
);
registerServiceWorker();
