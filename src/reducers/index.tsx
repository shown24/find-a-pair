import {StoreState} from "../types";
import {CHANGE_LEVEL, FIND_ALL_PAIRS, START_GAME, STOP_GAME} from '../constants';
import {ChangeLevel, GameAction} from "../actions";

export function game(state: StoreState, action: GameAction | ChangeLevel): StoreState {
  switch (action.type) {
    case START_GAME:
      return {...state, gameIsRunning: true};
    case STOP_GAME:
      return {...state, gameIsRunning: false, findAllPairs: false};
    case CHANGE_LEVEL:
      return {...state, difficultLevel: +action.payload, gameIsRunning: false};
    case FIND_ALL_PAIRS:
      return {...state, findAllPairs: true, gameIsRunning: false};
  }
  return state;
}
