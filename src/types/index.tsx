export interface StoreState {
  gameIsRunning: boolean
  difficultLevel: number
  findAllPairs: boolean
}
