import * as constants from '../constants'

export interface StartGame {
  type: constants.START_GAME
}

export interface StopGame {
  type: constants.STOP_GAME
}

export interface FindAllPairs {
  type: constants.FIND_ALL_PAIRS
}

export type GameAction = StartGame | StopGame | FindAllPairs;

export function startGame(): StartGame {
  return {
    type: constants.START_GAME
  }
}

export function stopGame(): StopGame {
  return {
    type: constants.STOP_GAME
  }
}

export function findAllPairs(): FindAllPairs {
  return {
    type: constants.FIND_ALL_PAIRS
  }
}
