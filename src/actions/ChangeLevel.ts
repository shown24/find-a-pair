import * as constants from '../constants'

export interface ChangeLevel {
  type: constants.CHANGE_LEVEL
  payload: number
}

export type LevelAction = ChangeLevel;

export function changeLevel(payload: number): ChangeLevel {
  return {
    type: constants.CHANGE_LEVEL,
    payload
  }
}
