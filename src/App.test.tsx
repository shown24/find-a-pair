import * as React from 'react';
import * as ReactDOM from 'react-dom';
import GameArea from './App';
import {Provider} from "react-redux";
import {createStore} from "redux";
import {StoreState} from "./types";
import {GameAction} from "./actions";
import {game} from "./reducers";

const store = createStore<StoreState, GameAction, null, null>(game, {
  gameIsRunning: false,
  difficultLevel: 6,
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}>
    <GameArea/>
  </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
