import * as React from "react";
import './index.css';

export interface MenuProps {
  difficultLevel: number
  gameIsRunning: boolean
  onChangeLevel: (level: number) => void
  onStartGame: () => void
}

export function Menu({difficultLevel, gameIsRunning, onStartGame, onChangeLevel}: MenuProps) {

  return (
    <div className="menu">
      <select className="menu_level" value={difficultLevel} onChange={e => onChangeLevel(+e.target.value)}>
        <option value="4">Easy</option>
        <option value="6">Medium</option>
        <option value="8">Hard</option>
      </select>
      <button className="menu_button" disabled={gameIsRunning} onClick={onStartGame}>Start Game</button>
    </div>
  )
}
