import * as React from "react";
import {find, filter, every} from 'lodash';
import "./index.css"

const classNames = require('classnames');

export interface ICard {
  id: number
  removed: boolean
  selected: boolean
  value: number
}

export interface CardsProps {
  difficultLevel: number
  gameIsRunning: boolean
  onFindAllPairs: () => void
}

export interface CardsState {
  cards: ICard[]
}

export class Cards extends React.Component<CardsProps, CardsState> {

  constructor(props: CardsProps) {
    super(props);
    this.state = {
      cards: []
    }
  }

  public refreshCards(size: number): void {
    this.setState({
      cards: this.generateGrid(size)
    })
  }

  public componentDidMount() {
    this.refreshCards(this.props.difficultLevel);
  }

  public componentWillReceiveProps(newProps: CardsProps): void {
    if (newProps.difficultLevel !== this.props.difficultLevel || !newProps.gameIsRunning) {
      this.refreshCards(newProps.difficultLevel);
    }
  }

  private generateGrid(size: number): ICard[] {
    let arr: ICard[] = [];
    let cardsCount = size * size;
    let cardValue: number = 1;
    while (cardsCount) {
      arr.push({id: cardsCount, removed: false, selected: false, value: cardValue});
      cardsCount % 2 && cardValue++;
      cardsCount--;
    }
    return this.mix(arr);
  }

  private mix(arr: ICard[]): ICard[] {
    return arr.sort(() => Math.random() - 0.5);
  }

  public compareCards(a: number, b: number): void {
    this.hideRemoveCards(
      find(this.state.cards, {id: a}).value === find(this.state.cards, {id: b}).value
    );
  }

  public hideRemoveCards(remove: boolean): void {
    setTimeout(() => {
      let arr = this.state.cards.slice(0);
      filter(arr, (el) => el.selected).forEach(el => {
        remove && (el.removed = true);
        el.selected = false;
      });
      this.setState({
        cards: arr
      });
      every(arr, ['removed', true]) && this.props.onFindAllPairs();
    }, 500);
  }

  public cardClick(id: number): void {
    let selected = filter(this.state.cards, (el) => el.selected);
    if (selected.length < 2) {
      let newCards = this.state.cards.slice(0);
      find(newCards, {id}).selected = true;
      this.setState({
        cards: newCards
      });
      selected.length === 1 && this.compareCards(selected[0].id, id);
    }
  }

  public render(): JSX.Element[] {
    return this.state
      .cards.map((c, i) => <div key={i}
                                onClick={
                                  (c.selected || c.removed || !this.props.gameIsRunning)
                                    ? () => false
                                    : () => this.cardClick(c.id)
                                }
                                className={classNames(
                                  'card',
                                  {'selected': c.selected},
                                  {'removed': c.removed}
                                )}>{c.selected ? c.value : ''}</div>)

  }
}
