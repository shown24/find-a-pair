import * as React from "react";
import "./index.css";
import Timer = NodeJS.Timer;

export interface TimerProps {
  gameIsRunning: boolean
  findAllPairs: boolean
  onStopGame: () => void
}

export interface TimerState {
  timeLeft: number
}

export class Counter extends React.Component<TimerProps, TimerState> {

  private timer: Timer;

  constructor(props: TimerProps) {
    super(props);
    this.state = {timeLeft: 30};
  }

  public componentWillUnmount(): void {
    clearInterval(this.timer);
  }

  public componentWillReceiveProps(newProps: TimerProps): void {
    newProps.gameIsRunning ? this.startCounter() : this.stopCounter();
    if (newProps.findAllPairs) {
      this.stopCounter();
      this.gameEnd(this.state.timeLeft);
    }
  }

  public gameEnd(time: number): void {
    this.stopCounter();
    this.props.onStopGame();
    alert(`You ${time ? 'WIN' : 'LOSE'}!!! Your sroce: ${time}`);
  }

  public startCounter(): void {
    this.timer = setInterval(this.tick, 1000);
  }

  public resetTime(): void {
    this.setState({timeLeft: 30})
  }

  public stopCounter(): void {
    clearInterval(this.timer);
    this.resetTime();
  }

  private tick = (): void => {
    return this.state.timeLeft > 0 ? this.setState({timeLeft: this.state.timeLeft - 1}) : this.gameEnd(0);
  };

  public render(): JSX.Element {
    return (
      <div className="counter">Time left: {this.state.timeLeft}s</div>
    );
  }
}
