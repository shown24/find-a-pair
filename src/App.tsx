import * as React from "react";
import "./App.css";
import Menu from "./containers/Menu";
import Counter from "./containers/Counter";
import Cards from "./containers/Cards";

export default function GameArea() {
  return (
    <div className="game">
      <div className="game_menu">
        <Menu/>
      </div>
      <div className="game_timer">
        <Counter/>
      </div>
      <div className="game_cards">
        <Cards/>
      </div>
    </div>
  );
}
