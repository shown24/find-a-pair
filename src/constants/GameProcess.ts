export const START_GAME = 'Start Game!';
export type START_GAME = typeof START_GAME;

export const STOP_GAME = 'Stop Game!';
export type STOP_GAME = typeof STOP_GAME;

export const FIND_ALL_PAIRS = 'Find All Pairs!';
export type FIND_ALL_PAIRS = typeof FIND_ALL_PAIRS;
